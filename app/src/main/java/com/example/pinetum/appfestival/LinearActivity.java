package com.example.pinetum.appfestival;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;

/**
 * Created by Pinetum on 2015/1/22.
 */
public class LinearActivity extends Activity{
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_linear_vertical);


    }
    @Override
    public void finish() {
        Intent responseIntent = new Intent();
        responseIntent.putExtra("returnFrom","LinearLayoutActivity");
        setResult(RESULT_OK,responseIntent);
        super.finish();
    }

}
