package com.example.pinetum.appfestival;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;

/**
 * Created by Pinetum on 2015/1/22.
 */
public class TableLayoutActivity extends Activity{
    @Override
    public void finish() {
        Intent responserIntent = new Intent();
        responserIntent.putExtra("returnFrom","TableLayoutActivity");
        setResult(RESULT_OK,responserIntent);
        super.finish();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tablelayout);
    }

}
