package com.example.pinetum.appfestival;

import android.app.Activity;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;

import org.json.JSONArray;
import org.w3c.dom.Text;

/**
 * Created by Pinetum on 2015/1/28.
 */
public class BooklistActivity extends Activity {
    final   String              ServerURL = "http://images.memobook.com.tw/m/api.php";
    private String              m_str_bookClass = "1";
    private ListView            m_ListView;
    private BooklistAdapter     m_adapter;
    private AsyncHttpClient     m_asy_httClient;
    private ProgressDialog      m_pgDiag;
    private DisplayImageOptions m_Dio;
    private TextView            m_textBack;
    private TextView            m_textTitle;
    private ImageLoader         m_img_loader = ImageLoader.getInstance();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_books_classbooks);
        m_str_bookClass = getIntent().getStringExtra("class_id");
        m_textBack      = (TextView) findViewById(R.id.id_textView_back2classes);
        m_textTitle     = (TextView) findViewById(R.id.id_textView_inclass_title);
        m_ListView      = (ListView)findViewById(R.id.id_listView_books);
        m_textTitle.setText(getIntent().getStringExtra("title"));
        m_textBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        m_Dio = new DisplayImageOptions.Builder()
                .showImageForEmptyUri(R.drawable.ic_launcher)
                .cacheInMemory(true)
                .cacheOnDisk(false)
                .build();
        m_img_loader.init(
                new ImageLoaderConfiguration.Builder(BooklistActivity.this)
                        .defaultDisplayImageOptions(m_Dio)
                        .build());

        m_asy_httClient = new AsyncHttpClient();
        m_pgDiag        = new ProgressDialog(BooklistActivity.this);
        m_pgDiag.setMessage("Loading");
        m_pgDiag.setCancelable(false);
        m_pgDiag.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
        m_pgDiag.show();
        RequestParams mpara = new RequestParams();
        mpara.add("act", "memo_class");
        mpara.add("id", m_str_bookClass);
        m_asy_httClient.get(ServerURL, mpara, new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(JSONArray response) {
                m_pgDiag.dismiss();
                m_adapter = new BooklistAdapter(response, getBaseContext(), m_img_loader);
                m_ListView.setAdapter(m_adapter);


                super.onSuccess(response);

            }

            @Override
            public void onProgress(int bytesWritten, int totalSize) {
                m_pgDiag.setMax(totalSize);
                m_pgDiag.setProgress(bytesWritten);
                super.onProgress(bytesWritten, totalSize);
            }

            @Override
            public void onFailure(Throwable e, JSONArray errorResponse) {
                m_pgDiag.dismiss();
                super.onFailure(e, errorResponse);
            }
        });



    }

}



    /*
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }
}
*/