package com.example.pinetum.appfestival;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

/**
 * Created by Pinetum on 2015/1/22.
 */
public class FrameActivity extends Activity{
    @Override
    protected void onStop() {
        super.onStop();
        Log.i("A","onStop");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.i("A","onDestroy");
    }

    @Override
    protected void onPause() {
        super.onPause();
        Log.i("A","Pause");
    }

    @Override
    protected void onStart() {
        super.onStart();
        Log.i("A","onStart");
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        Log.i("A","onReStart");
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.i("A","onResume");
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_framelayout);
        Log.i("A","onCreate");
    }
    @Override
    public void finish() {
        Intent responserIntent = new Intent();
        responserIntent.putExtra("returnFrom","FrameLayoutActivity");
        setResult(RESULT_OK,responserIntent);
        super.finish();
    }

}
