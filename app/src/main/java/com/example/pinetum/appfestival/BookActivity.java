package com.example.pinetum.appfestival;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.ViewFlipper;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;

import org.json.JSONArray;
import org.json.JSONObject;

/**
 * Created by Pinetum on 2015/1/28.
 */
public class BookActivity extends Activity {
    final   String              ServerURL = "http://images.memobook.com.tw/m/api.php";
    private ViewFlipper         m_VP_pags;
    private TextView            m_text_bookInfo;
    private ProgressDialog      m_pgDiag;
    private String              m_str_imgUrl;
    private ImageView[]         m_imgV_pages;
    private DisplayImageOptions m_Dio;
    private ImageLoader         m_img_loader = ImageLoader.getInstance();
    private ImageView           m_img_Book_story;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_single_book);

        m_Dio = new DisplayImageOptions.Builder()
                .showImageForEmptyUri(R.drawable.ic_launcher)
                .cacheInMemory(true)
                .cacheOnDisk(false)
                .build();
        m_img_loader.init(
                new ImageLoaderConfiguration.Builder(BookActivity.this)
                        .defaultDisplayImageOptions(m_Dio)
                        .build());


        m_VP_pags           = (ViewFlipper)     findViewById(R.id.id_viewFlipper_pages);
        m_text_bookInfo     = (TextView)        findViewById(R.id.id_textView_book_info);
        m_img_Book_story    = (ImageView)       findViewById(R.id.id_imageView_book_story);
        AsyncHttpClient httpClient = new AsyncHttpClient();
        RequestParams mp =  new RequestParams();
        m_pgDiag        = new ProgressDialog(BookActivity.this);
        m_pgDiag.setMessage("Loading");
        m_pgDiag.setCancelable(false);
        m_pgDiag.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
        m_pgDiag.show();

        mp.add("act", "memo_detail");
        mp.add("id", getIntent().getStringExtra("id"));
        m_str_imgUrl = getIntent().getStringExtra("imgUrl");
        m_img_loader.displayImage(m_str_imgUrl, m_img_Book_story);
        httpClient.get(ServerURL, mp, new JsonHttpResponseHandler(){
            @Override
            public void onSuccess(JSONObject response) {
                try{
                    m_pgDiag.dismiss();
                    JSONObject detail   = response.getJSONObject("details");
                    JSONArray imgs     = response.getJSONArray("imgs");
                    m_imgV_pages        = new ImageView[imgs.length()];
                    m_text_bookInfo.setText(detail.getString("title"));
                    for(int i = 0; i <imgs.length() ; i++){
                        String aaa = imgs.get(i).toString();
                        m_imgV_pages[i] = new ImageView(BookActivity.this);
                        m_img_loader.displayImage(imgs.get(i).toString(), m_imgV_pages[i]);
                        m_VP_pags.addView(m_imgV_pages[i]);

                    }
                    m_pgDiag.dismiss();


                }catch (Exception e){e.printStackTrace();}


                super.onSuccess(response);
            }

            @Override
            public void onProgress(int bytesWritten, int totalSize) {
                m_pgDiag.setMax(totalSize);
                m_pgDiag.setProgress(bytesWritten);
                super.onProgress(bytesWritten, totalSize);
            }

            @Override
            public void onFailure(Throwable e, JSONArray errorResponse) {
                m_pgDiag.dismiss();
                super.onFailure(e, errorResponse);
            }
        });
        m_VP_pags.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                m_VP_pags.showNext();
                return false;
            }
        });
    }
}
