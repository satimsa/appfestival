package com.example.pinetum.appfestival;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

/**
 * Created by Pinetum on 2015/1/23.
 */
public class UIpraticeActivity extends Activity {

    private EditText            m_EditText_name;
    private Button              m_btn_toast;
    private RadioGroup          m_radioGroup_age;
    private RadioButton         m_radbtn_20_25;
    private RadioButton         m_radbtn_26_30;
    private RadioButton         m_radbtn_31_50;
    private TextView            m_textView_showAge;
    private SharedPreferences   m_SharedData;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_data_base_ui);
        m_SharedData            = getSharedPreferences("data", Context.MODE_PRIVATE);
        m_EditText_name         = (EditText)    findViewById(R.id.id_editText_name);
        m_btn_toast             = (Button)      findViewById(R.id.id_btn_Toast);
        m_radioGroup_age        = (RadioGroup)  findViewById(R.id.id_RadioGroup_age);
        m_radbtn_20_25          = (RadioButton) findViewById(R.id.id_radioButton_age_20_25);
        m_radbtn_26_30          = (RadioButton) findViewById(R.id.id_radioButton_age_26_30);
        m_radbtn_31_50          = (RadioButton) findViewById(R.id.id_radioButton_age_31_50);
        m_textView_showAge      = (TextView)    findViewById(R.id.id_textView_show_age);


        m_EditText_name.setText(m_SharedData.getString("name", ""));
        m_textView_showAge.setText((m_SharedData.getString("age", "")));


        //按下按鈕就toast出姓名
        m_btn_toast.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                new AlertDialog.Builder(UIpraticeActivity.this)
                        .setTitle("是否存擋？")
                        .setMessage("Your Name:"+m_EditText_name.getText().toString()+"\nYour age:"+m_textView_showAge.getText().toString())
                        .setPositiveButton("是", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                m_SharedData.edit()
                                        .putString("name", m_EditText_name.getText().toString())
                                        .putString("age", m_textView_showAge.getText().toString())
                                        .commit();
                                Toast.makeText(getApplicationContext(), "Your name is " + m_EditText_name.getText().toString(), Toast.LENGTH_LONG).show();

                            }
                        })

                        .setNegativeButton("否",null)
                        .show();
            }

        });
        //監聽radioGroup是否被變換或勾選
        m_radioGroup_age.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                switch (checkedId){
                    case R.id.id_radioButton_age_20_25:
                        m_textView_showAge.setText("20~25");
                        break;
                    case R.id.id_radioButton_age_26_30:
                        m_textView_showAge.setText("26~30");
                        break;
                    case R.id.id_radioButton_age_31_50:
                        m_textView_showAge.setText("31~50");
                        break;

                }

            }
        });
    }
}
