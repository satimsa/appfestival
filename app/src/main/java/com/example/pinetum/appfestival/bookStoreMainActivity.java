package com.example.pinetum.appfestival;

import android.app.Activity;
import android.gesture.Gesture;
import android.os.Bundle;
import android.util.Log;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.ViewFlipper;

/**
 * Created by Pinetum on 2015/1/26.
 */
public class bookStoreMainActivity extends Activity {
    private ViewFlipper     m_vp_books;
    private ImageView[]     m_imgView_imgs;
    private int[]           m_n_imgResc = {R.drawable.news1, R.drawable.news2, R.drawable.news3};
    private GestureDetector myDet;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bookstore_main);
        m_vp_books      = (ViewFlipper)     findViewById(R.id.id_viewFliper_book);
        m_imgView_imgs  = new ImageView[3];
        for(int i = 0 ; i < m_imgView_imgs.length ; i++){
            m_imgView_imgs[i] = new ImageView(bookStoreMainActivity.this);
            m_imgView_imgs[i].setImageDrawable(getResources().getDrawable(m_n_imgResc[i]));
            m_vp_books.addView(m_imgView_imgs[i]);
        }

        myDet =  new GestureDetector(this,
                        new GestureDetector.SimpleOnGestureListener(){
                            @Override
                            public boolean onDown(MotionEvent e) {
                                return true;
                            }

                            @Override
                            public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY) {
                                if(velocityX <-10)
                                    m_vp_books.showNext();
                                else if(velocityX > 10)
                                    m_vp_books.showPrevious();
                                Log.i("GDL",""+velocityX);
                                return super.onFling(e1, e2, velocityX, velocityY);
                            }
                        });
        m_vp_books.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {

                return myDet.onTouchEvent(event);
            }
        });
        m_vp_books.setAnimation(AnimationUtils.loadAnimation(bookStoreMainActivity.this, R.anim.abc_slide_out_top));



    }

}
