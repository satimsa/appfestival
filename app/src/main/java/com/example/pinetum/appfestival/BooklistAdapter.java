package com.example.pinetum.appfestival;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.ImageLoader;

import org.json.JSONArray;

import java.net.URLDecoder;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Pinetum on 2015/1/28.
 */
public class BooklistAdapter extends BaseAdapter
{

    private JSONArray       m_data;
    private Context         m_Context;
    private ImageLoader     m_imgLoader;
    private HashMap         imgUrl;
    LayoutInflater m_LayoutInflater;
    BooklistAdapter(JSONArray data, Context context, ImageLoader imgLoader){
        m_imgLoader = imgLoader;
        m_data = data;
        m_Context = context;
        m_LayoutInflater = LayoutInflater.from(context);
        imgUrl = new HashMap();
        imgUrl.clear();

    }
    @Override
    public int getCount() {
        return m_data.length()/2;
    }

    @Override
    public Object getItem(int position) {

        try{
            return m_data.getJSONObject(position);
        }catch (Exception e){e.printStackTrace();}
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View dataView = convertView;
        if(convertView == null){

            dataView =  m_LayoutInflater.inflate(R.layout.list_cell_class_books,null);
            TextView textViewL =(TextView) dataView.findViewById(R.id.id_textView_books_l);
            TextView textViewR =(TextView) dataView.findViewById(R.id.id_textView_books_r);

            ImageView ig = (ImageView)dataView.findViewById(R.id.id_imageView_books_l);
            ig.setTag(String.valueOf(position*2));
            ImageView igr= (ImageView)dataView.findViewById(R.id.id_imageView_books_r);
            igr.setTag(position*2+1);

            //AsyncHttpClient mah = new AsyncHttpClient();
            //mah.get(m_data[position])


            try {
                textViewL.setText(URLDecoder.decode(m_data.getJSONObject(position*2).getString("title")+"\n"+m_data.getJSONObject(position*2+1).getString("anthor")));
                textViewR.setText(URLDecoder.decode(m_data.getJSONObject(position*2+1).getString("title")+"\n"+m_data.getJSONObject(position*2+1).getString("anthor")));
                m_imgLoader.displayImage(m_data.getJSONObject(position * 2).getString("img"), ig);
                m_imgLoader.displayImage(m_data.getJSONObject(position*2+1).getString("img"), igr);
                ig.setTag(m_data.getJSONObject(position*2).getString("id"));
                igr.setTag(m_data.getJSONObject(position*2+1).getString("id"));
                imgUrl.put(m_data.getJSONObject(position*2).getString("id"), m_data.getJSONObject(position * 2).getString("img"));
                imgUrl.put(m_data.getJSONObject(position*2+1).getString("id"), m_data.getJSONObject(position * 2+1).getString("img"));
            }catch (Exception e){e.printStackTrace();}

            View.OnClickListener mclick = new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent go2Book = new Intent(m_Context, BookActivity.class);
                    go2Book.putExtra("id", v.getTag().toString());
                    go2Book.putExtra("imgUrl",imgUrl.get(v.getTag().toString()).toString());
                    go2Book.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    m_Context.startActivity(go2Book );
                }
            };
            ig.setOnClickListener(mclick);
            igr.setOnClickListener(mclick);
            //ig.setImageDrawable(m_Context.getResources().getDrawable(R.drawable.news1));
        }

        return dataView;
    }

}
