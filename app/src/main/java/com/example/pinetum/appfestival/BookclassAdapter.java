package com.example.pinetum.appfestival;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.ImageLoader;

import org.json.JSONArray;

import java.net.URLDecoder;

/**
 * Created by Pinetum on 2015/1/27.
 */
public class BookclassAdapter extends BaseAdapter
{

    private JSONArray m_data;
    private Context m_Context;
    private ImageLoader m_imgLoader;
    LayoutInflater m_LayoutInflater;
    BookclassAdapter(JSONArray data, Context context, ImageLoader imgLoader){
        m_imgLoader = imgLoader;
        m_data = data;
        m_Context = context;
        m_LayoutInflater = LayoutInflater.from(context);
    }
    @Override
    public int getCount() {
        return m_data.length();
    }

    @Override
    public Object getItem(int position) {

        try{
            return m_data.getJSONObject(position);
        }catch (Exception e){e.printStackTrace();}
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View dataView = convertView;
        if(convertView == null){

            dataView =  m_LayoutInflater.inflate(R.layout.list_cell_classes,null);
            TextView ii =(TextView) dataView.findViewById(R.id.id_textView_l);
            TextView iiCount =(TextView) dataView.findViewById(R.id.id_textView_r);

            ImageView ig = (ImageView)dataView.findViewById(R.id.id_imagView_l);
            ig.setTag(String.valueOf(position*2));
            ImageView igr= (ImageView)dataView.findViewById(R.id.id_imageView_r);
            igr.setTag(position*2+1);

            //AsyncHttpClient mah = new AsyncHttpClient();
            //mah.get(m_data[position])


            try {
                ii.setText(URLDecoder.decode(m_data.getJSONObject(position).getString("title")));
                iiCount.setText(URLDecoder.decode(m_data.getJSONObject(position).getString("counts")));
                m_imgLoader.displayImage(m_data.getJSONObject(position).getString("icon_bg"), igr);
                m_imgLoader.displayImage(m_data.getJSONObject(position).getString("icon"), ig);
                dataView.setTag(m_data.getJSONObject(position).getString("id"));
            }catch (Exception e){e.printStackTrace();}

            //ig.setImageDrawable(m_Context.getResources().getDrawable(R.drawable.news1));
        }

        return dataView;
    }

}
