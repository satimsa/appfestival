package com.example.pinetum.appfestival;


/*

    code 越寫越醜Orz  APP玩樂節！2015/1/22~2015/1/28
    day1:layout, textView, Button, ButtonListener
    day2:Intent, Activities data transform, thread, UI thread, progress dialog, SharedPreferences, Toast
    day3:SharedPreferences Dialog,  ImageView
 */
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Handler;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;


public class MainActivity extends ActionBarActivity {

    private Button                  m_btn_go2baseUI, m_btn_autoCount, m_btn_call, m_btn_add, m_btn_sub, m_btn_go2linear, m_btn_go2table, m_btn_go2frame,m_btn_go2url;
    private Context                 m_contenxt_mainAty;
    private int                     m_n_handCounter = 0;
    private View.OnClickListener    m_OCL_main;
    private TextView                m_textV_show;
    private EditText                m_edText_url, m_editText_phone;
    private Handler                 m_handler_countUpdateUI;
    private int                     m_n_timeCounter = 0;
    private  final int              m_n_totalCount  = 20;
    private ProgressDialog          m_pd_count;

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        Log.i("onresult","requestCode:"+requestCode+"resultCode:"+resultCode);
        if(resultCode == RESULT_OK){
            String strWhere = data.getStringExtra("returnFrom");
            m_textV_show.setText(strWhere);
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);
        m_textV_show            = (TextView)    findViewById(R.id.id_TextView1);
        m_btn_add               = (Button)      findViewById(R.id.id_btn_add);
        m_btn_sub               = (Button)      findViewById(R.id.id_btn_sub);
        m_btn_go2table          = (Button)      findViewById(R.id.id_btn_tblaeL);
        m_btn_go2linear         = (Button)      findViewById(R.id.id_btn_go2linear);
        m_btn_go2frame          = (Button)      findViewById(R.id.id_btn_go2frame);
        m_btn_call              = (Button)      findViewById(R.id.id_btn_call);
        m_btn_go2url            = (Button)      findViewById(R.id.id_btn_go2url);
        m_btn_autoCount         = (Button)      findViewById(R.id.id_btn_autoCount);
        m_btn_go2baseUI         = (Button)      findViewById(R.id.id_btn_go2baseUI);
        m_edText_url            = (EditText)    findViewById(R.id.id_edtext_url);
        m_editText_phone        = (EditText)    findViewById(R.id.id_editText_phone_num);

        m_handler_countUpdateUI = new Handler();
        m_pd_count              = new ProgressDialog(MainActivity.this);
        m_contenxt_mainAty      = getApplicationContext();
        m_OCL_main              = //counter + or -
        new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(v.getId() == R.id.id_btn_add)
                    m_n_handCounter ++;
                else if(v.getId() == R.id.id_btn_sub && m_n_handCounter > 0)
                    m_n_handCounter --;
                //Toast mShowCounter = Toast.makeText(m_contenxt_mainAty, String.valueOf(m_n_counter), Toast.LENGTH_SHORT);
                //mShowCounter.show();
                m_textV_show.setText(String.valueOf(m_n_handCounter));
            }
        };
        //progress dialog
        m_pd_count.setTitle("Counting");
        m_pd_count.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
        m_pd_count.setMessage("Count by thread");
        m_pd_count.setMax(m_n_totalCount);
        //將button 加入listener
        m_btn_add.setOnClickListener(m_OCL_main);
        m_btn_sub.setOnClickListener(m_OCL_main);
        m_textV_show.setTextSize(40);
        m_btn_go2linear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent linearActivity = new Intent(getBaseContext(), LinearActivity.class);
                startActivityForResult(linearActivity, 1);//指定request code
            }
        });
        m_btn_go2table.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent tableActivity = new Intent(getBaseContext(), TableLayoutActivity.class);
                startActivityForResult(tableActivity, 2);
            }
        });
        m_btn_go2frame.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent FrameActivity = new Intent(getBaseContext(), FrameActivity.class);
                startActivityForResult(FrameActivity, 3);
            }
        });
        m_btn_go2url.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.i("getURL",m_edText_url.getText().toString());
                //Intent openBrowswer = new Intent();
                Uri targetUri = Uri.parse(m_edText_url.getText().toString());
                //openBrowswer.setAction(Intent.ACTION_VIEW);
                //openBrowswer.setData(targetUri);
                Intent it = new Intent(Intent.ACTION_VIEW,targetUri);
                startActivity(it);
            }
        });
        m_btn_call.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.i("getPhone",m_editText_phone.getText().toString());
                startActivity(new Intent(Intent.ACTION_DIAL,Uri.parse(m_editText_phone.getText().toString())));
            }
        });
        m_btn_go2baseUI.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getBaseContext(),UIpraticeActivity.class));
            }
        });
        m_btn_autoCount.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                m_n_timeCounter = 0 ;
                m_textV_show.setText(String.valueOf(m_n_timeCounter));
                m_pd_count.show();
                Runnable timeTasek = new Runnable() {
                    @Override
                    public void run() {

                        while(m_n_timeCounter < m_n_totalCount){
                            try {
                                m_pd_count.setProgress(m_n_timeCounter);
                                Thread.sleep(500);
                                m_handler_countUpdateUI.post(new Runnable() {
                                    @Override
                                    public void run() {
                                        m_pd_count.setProgress(m_n_timeCounter);
                                        m_textV_show.setText(String.valueOf(m_n_timeCounter));
                                    }
                                });
                            }catch(Exception e){Log.e("thread", e.toString());}
                            m_n_timeCounter++;
                        }
                        Log.i("thread","thread finish");
                        m_pd_count.dismiss();
                    }
                };

                new Thread(timeTasek).start();


            }
        });
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items
        // to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
